import { heroes } from '../index';

export const getHeroesByPublisher = (publisher) => {
  const validaPublishers = ['DC Comics', 'Marvel Comics'];
  if (!validaPublishers.includes(publisher)) {
    throw new Error(`${publisher} is not a valid publisher`);
  }

  return heroes.filter((heroe) => heroe.publisher === publisher);
};
