import { Routes, Route } from 'react-router-dom';

import { HeroesRoutes } from '../heroes/index.js';
import { LoginPage } from '../auth/index.js';

export const AppRouter = () => {
  return (
    <>
      <Routes>
        <Route path='login' element={<LoginPage />} />
        <Route path='/*' element={<HeroesRoutes />} />
      </Routes>
    </>
  );
};
